document.addEventListener('DOMContentLoaded', () => {

    // obtiene todos los elementos "navbar burger"
    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    // comprueba si existe al menos un navbar burger
    if ($navbarBurgers.length > 0) {

        // le asigna un evento 'click'
        $navbarBurgers.forEach(el => {
            el.addEventListener('click', () => {

                // obtiene el target del atributo 'data target'
                const target = el.dataset.target;
                const $target = document.getElementById(target);

                // activa o desactiva la clase 'is-active' según corresponda
                el.classList.toggle('is-active');
                $target.classList.toggle('is-active');

            });
        });
    }

});
